<?php
class koneksi {

    public $koneksi;
    protected $server;
    protected $user;
    protected $pass;
    protected $db;

    public function __construct() {
        $this->variable();
        $this->set();
        $this->get();
    }

    protected function variable() {
        $this->server = 'localhost';
        $this->user = 'root';
        $this->pass = '';
        $this->db = 'testskill';
    }

    protected function set() {
        try {
           $this->koneksi = mysqli_connect($this->server, $this->user, $this->pass, $this->db);
        }
        catch(Exception $e) {
            echo "function setConnection() Error: ".$e;
        }
    }

    public function get() {
        return $this->koneksi;
    }

    public function pesan() {
        if($this->koneksi) echo 'koneksi berhasil';
        else echo 'koneksi terputus';
    }

}
?>