<?php
class controller extends koneksi {

    public function getReportProduct(int $type, string $where = null) {
        if(!is_null($where) || $where != '' || $where != null) $where = "where ".$where;

        $result = $this->get()->query("SELECT * from report_product $where");
        $data = array();
        if($type > 1) {
            while($row = $result->fetch_array()) {
                $data[] = $row;
            }
        }
        else {
            $row = $result->fetch_array();
            $data[] = $row;
        }

        return $data;
    }

    public function getStore(int $type, string $where = null) {
        if(!is_null($where) || $where != '' || $where != null) $where = "where ".$where;

        $result = $this->get()->query("SELECT * from store $where");
        $data = array();
        if($type > 1) {
            while($row = $result->fetch_array()) {
                $data[] = $row;
            }
        }
        else {
            $row = $result->fetch_array();
            $data[] = $row;
        }

        return $data;
    }

    public function getStoreArea(int $type, string $where = null) {
        if(!is_null($where) || $where != '' || $where != null) $where = "where ".$where;

        $result = $this->get()->query("SELECT * from store_area $where");
        $data = array();
        if($type > 1) {
            while($row = $result->fetch_array()) {
                $data[] = $row;
            }
        }
        else {
            $row = $result->fetch_array();
            $data[] = $row;
        }

        return $data;
    }

    public function getProductBrand(int $type, string $where = null) {
        if(!is_null($where) || $where != '' || $where != null) $where = "where ".$where;

        $result = $this->get()->query("SELECT * from product_brand $where");
        $data = array();
        if($type > 1) {
            while($row = $result->fetch_array()) {
                $data[] = $row;
            }
        }
        else {
            $row = $result->fetch_array();
            $data[] = $row;
        }

        return $data;
    }


    public function getDataBarChart($id, $datefrom, $dateto, $debug=null) {
        $query = "SELECT   a.area_name as area, 
                            (sum(c.compliance)/count(c.compliance)*100) as compliance
                    from store_area as a 
                    inner join store as b 
                        on a.area_id = b.area_id
                    inner join report_product as c 
                        on b.store_id = c.store_id
                    where   a.area_id = $id and 
                            c.tanggal between '$datefrom' and '$dateto'";

        $result = $this->get()->query("SELECT   a.area_name as area, 
                                                (sum(c.compliance)/count(c.compliance)*100) as compliance
                                        from store_area as a 
                                        inner join store as b 
                                            on a.area_id = b.area_id
                                        inner join report_product as c 
                                            on b.store_id = c.store_id
                                        where   a.area_id = $id and 
                                                c.tanggal between '$datefrom' and '$dateto'");
        $data = array();
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        if($debug == 'query') return $query;
        else if($debug == 'row') return $result->num_rows;
        return $data;
    }

    public function getBrandTable($id, $datefrom, $dateto, $debug=null) {
        $query = "SELECT DISTINCT   a.area_name as area, 
                                    e.brand_name as brand_name, 
                                    (SELECT (sum(ce.compliance)/count(ce.compliance)*100)
                                    from report_product as ce
                                        inner join product as de
                                            on ce.product_id = de.product_id
                                        inner join product_brand as ee
                                            on de.brand_id = ee.brand_id
                                        where e.brand_id = ee.brand_id and ce.tanggal between '$datefrom' and '$dateto'
                                    ) as compliant
                    from store_area as a 
                    inner join store as b 
                    on a.area_id = b.area_id
                    inner join report_product as c 
                    on b.store_id = c.store_id
                    inner join product as d 
                    on c.product_id = d.product_id
                    inner join product_brand as e 
                    on d.brand_id = e.brand_id
                    where a.area_id = $id and c.tanggal between '$datefrom' and '$dateto'";

        $result = $this->get()->query("SELECT DISTINCT  a.area_name as area, 
                                                        e.brand_name as brand, 
                                                        (SELECT (sum(ce.compliance)/count(ce.compliance)*100)
                                                        from report_product as ce
                                                            inner join product as de
                                                                on ce.product_id = de.product_id
                                                            inner join product_brand as ee
                                                                on de.brand_id = ee.brand_id
                                                            where e.brand_id = ee.brand_id and ce.tanggal between '$datefrom' and '$dateto'
                                                        ) as compliance
                                        from store_area as a 
                                        inner join store as b 
                                            on a.area_id = b.area_id
                                        inner join report_product as c 
                                            on b.store_id = c.store_id
                                        inner join product as d 
                                            on c.product_id = d.product_id
                                        inner join product_brand as e 
                                            on d.brand_id = e.brand_id
                                        where a.area_id = $id and c.tanggal between '$datefrom' and '$dateto'");
        $data = array();
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $data[] = $row;
        }

        if($debug == 'query') return $query;
        else if($debug == 'row') return $result->num_rows;
        return $data;
    }
}
?>