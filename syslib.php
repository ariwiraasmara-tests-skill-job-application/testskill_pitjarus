<?php
class syslib {

    // GET AND POST PARAMETER
    public function POST(string $str=null) {
        return @$_POST[$str];
    }
    
    public function GET(string $str=null) {
        return @$_GET[$str];
    }

    // SAFETY CONNECTION STRING FROM INJECTION
    public function escape(String|float $str=null) {
        try {
            return htmlspecialchars(htmlentities(addslashes($str)));
        }
        catch(Exception $e) {
            //return "Error Function escape() : ".$e;
            return "";
        }
    }

    public function safe(String|float $str=null) {
        try {
            //return mysqli_real_escape_string($this->getConnection(), $this->escape($str));
            return $this->getConnection()->real_escape_string($str);
        }
        catch(Exception $e) {
            //return "Error Function safe() : ".$e;
            return "";
        }
    }


    // READABLE FORMAT TEXT
    public function readable(String|float $str=null) {
        try {
            return html_entity_decode(htmlspecialchars_decode($str));
        }
        catch(Exception $e) {
            //return "Error Function READABLE() : ".$e;
            return "";
        }
    }

    public function formatnumber(int|float $str=null, $koma=0) {
        try {
            return number_format($str, $koma, ',', '.');
        }
        catch(Exception $e) {
            //return "Error Function formatnumber() : ".$e;
            return "";
        }
    }

    public function rupiah(int|float $str=null) {
        try {
            return 'Rp. '.$this->formatnumber($str, 2);
        }
        catch(Exception $e) {
            //return "Error Function Rupiah() : ".$e;
            return "";
        }
    }

}
?>