<?php 
require('require.php'); 
?>

<html>
    <head>
        <title>Test Skill Pitjarus</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<meta name="keywords" content="Syahri Ramadhan Wiraasmara's Private Website, Personal Use Only" />
				
		<meta content="IE=edge" http-equiv="x-ua-compatible">
		<meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
		<meta content="yes" name="apple-mobile-web-app-capable">
		<meta content="yes" name="apple-touch-fullscreen">

    	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine"/>
    	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
    
    	<link href="bulma/css/bulma-rtl.css" rel="stylesheet" type="text/css">
        <link href="bulma/css/bulma-rtl.min.css" rel="stylesheet" type="text/css">
        <link href="bulma/css/bulma.css" rel="stylesheet" type="text/css">
        <link href="bulma/css/bulma.min.css" rel="stylesheet" type="text/css">
        <link href="bulma/graph/custom.css" rel="stylesheet" type="text/css">
        <link href="bulma/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="is-widescreen">

            <!-- Form -->
            <section class="section">
                <form action="" method="post">
                    <div class="columns is-mobile">
                        <div class="column">
                            <div class="control">
                                <div class="select is-normal is-responsive is-fullwidth">
                                    <select id="selectarea" name="selectarea" required>
                                        <option value="" disabled selected>-- Select Area --</option>
                                        <?php
                                        foreach($dsa as $sa) { ?>
                                            <option value="<?php echo $sa['area_id']; ?>"><?php echo $sa['area_name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <input class="input is-responsive is-fullwidth" id="datefrom" name="datefrom" type="date" placeholder="Select dateFrom" required>
                        </div>

                        <div class="column">
                            <input class="input is-responsive is-fullwidth" id="dateto" name="dateto" type="date" placeholder="Select dateTo" required>
                        </div>

                        <div class="column">
                            <button type="submit" id="processing" name="processing" class="button is-link is-normal is-responsive is-fullwidth">View</button>
                        </div>
                    </div>
                </form>
            </section>
            
            <?php
            if( isset($_POST['processing']) ) { 
                $areaid     = $sys->POST('selectarea');
                $datefrom   = $sys->POST('datefrom');
                $dateto     = $sys->POST('dateto');

                $databarchart = $cont->getDataBarChart($areaid, $datefrom, $dateto);
                //var_dump($databarchart);
                //echo $databarchart[0][0];

                $dsa2 = $cont->getStoreArea(2, "area_id='$areaid'");
                $dpb2 = $cont->getProductBrand(2);
                $dbt = $cont->getBrandTable($areaid, $datefrom, $dateto);
                ?>
                <!-- Horizontal Bar Chart -->
                <section class="section">
                    <div class="graphs-container" data-title="<?php //echo $title.', '.$datefrom.' - '.$dateto; ?>">
                        <div class="data-container column is-full columns is-mobile is-size-7 has-text-white is-marginless">
                            <?php
                            foreach($databarchart as $bar) { ?>
                                <div data-title="<?php echo $bar['area'].', '.number_format($bar['compliance'], 0, ',', '.').'%'; ?>" data-value="<?php echo $bar['compliance']; ?>"></div>
                                <?php
                            }
                            ?>
                            <!--
                            <div data-title="One" data-value="10"></div>
                            <div data-title="Two" data-value="50"></div>
                            <div data-title="Three" data-value="100"></div>
                            <div data-title="Four" data-value="55"></div>
                            <div data-title="Five" data-value="25"></div>
                            <div data-title="Six" data-value="88"></div>
                            <div data-title="Seven" data-value="77"></div>
                            <div data-title="Eight" data-value="66"></div>
                            <div data-title="Nine" data-value="9"></div>
                            <div data-title="Ten" data-value="10"></div>
                            <div data-title="Eleven" data-value="11"></div>
                            <div data-title="Twelve" data-value="12"></div>
                            -->
                        </div>
                    </div>
                </section>
                
                <!-- Table -->
                <section class="section">
                    <table class="table is-fullwidth is-striped">
                        <thead>
                            <tr>
                                <th class="">Brand</th>
                                
                                <?php
                                foreach($dsa2 as $sa) { 
                                    echo '<th class="">'.$sa['area_name'].'</th>';
                                }
                                ?>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach($dbt as $brand) { ?>
                                <tr>
                                    <td><?php echo $brand['brand']; ?></th>
                                    <td><?php echo number_format($brand['compliance'], 2, ',', '.').'%'; ?></th>
                                </tr>
                                <?php 
                            }
                            ?>
                            
                        </tbody>
                    </table>
                </section>
                <?php
            }
            ?>
        
        </div>
    </body>

    <footer class="footer">
        <div class="content has-text-centered">
            <p>
                Develop by Syahri Ramadhan Wiraasmara <br/>
                <a href="https://gitlab.com/ariwiraasmara-tests-skill-job-application/testskill_pitjarus">Gitlab</a>
            </p>
        </div>

        <script src="bulma/graphs.js"></script>
    </footer>
</html>